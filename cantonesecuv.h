#ifndef CANTONESECUV_H
#define CANTONESECUV_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class CantoneseCUV; }
QT_END_NAMESPACE

class CantoneseCUV : public QMainWindow
{
    Q_OBJECT

public:
    CantoneseCUV(QWidget *parent = nullptr);
    ~CantoneseCUV();

private:
    Ui::CantoneseCUV *ui;
};
#endif // CANTONESECUV_H
