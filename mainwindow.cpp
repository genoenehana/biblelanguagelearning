#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QLabel>
#include <QPushButton>
#include "cantonesecuv.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // set the title of the main window
    setWindowTitle("Bible Language Learning");

    QLabel *label = new QLabel(this);

    // Set the text of the label
    label->setText("Welcome to the Bible Language Learning Application");
    // Set the position and size of the label
    label->setGeometry(QRect(QPoint(0, 0), QSize(400, 50)));

    // Create a push button
    QPushButton *button = new QPushButton("CantoneseCUV", this);

    // Set the position and size of the button
    button->setGeometry(QRect(QPoint(100, 100), QSize(200, 50)));

    // Create a new instance of the Cantonesecuv
    cantonesecuv = new CantoneseCUV();

    // Connect the button to a slot
    connect(button, SIGNAL(clicked()), this, SLOT(cantonesecuvButton()));
}

void MainWindow::cantonesecuvButton()
{
    // Handle the button click event here

    // Show the new window
    cantonesecuv->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}

