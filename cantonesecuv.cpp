#include "cantonesecuv.h"
#include "./ui_cantonesecuv.h"
#include <QLabel>
#include <QPushButton>

CantoneseCUV::CantoneseCUV(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::CantoneseCUV)
{
    ui->setupUi(this);

    // set the title of the main window
    setWindowTitle("Cantonese CUV");
}

CantoneseCUV::~CantoneseCUV()
{
    delete ui;
}

